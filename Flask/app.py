from flask import Flask, render_template
import sqlite3
import plotly
import time
import plotly.utils
import plotly.express as px
import json
import pandas as pd
from collections import Counter
import os
from subprocess import Popen
import sys

app = Flask(__name__,
            static_url_path='', 
            static_folder='static',
            template_folder='templates')

@app.route("/", methods=['GET','POST'])
def home():
    #Connexion à la db
    conn = sqlite3.connect('social_greenwave.db')  
    c = conn.cursor()

    #Récupération des articles de presse
    c.execute("SELECT * FROM news_articles")
    data = c.fetchall()
    list_titres = [row[0] for row in data]
    list_des = [row[1] for row in data]
    list_src = [row[2] for row in data]
    list_publi = [row[3] for row in data]
    list_link = [row[4] for row in data] 
    list_img = [row[5] for row in data]

    #Récupération des tweets les plus populaires
    c.execute("SELECT * ,(favorite_count + retweet_count) as impact FROM tweets_recup ORDER BY impact DESC LIMIT 6")
    data = c.fetchall()

    list_creatime = [row[1] for row in data]
    list_username = [row[2] for row in data]
    list_tweets = [row[4] for row in data]
    #list_sentiment = [row[5] for row in data]
    list_impact = [row[10] for row in data]
    
    #Graph1:
    #Récupération du comptes des hashtags sur les tweets en live
    c.execute("SELECT hashtags FROM tweets_live WHERE hashtags != ''")
    data = c.fetchall()
    list_hashtags = [row[0] for row in data]

    #On sépare les hashtags pour les tweets qui en avait plusieurs
    list_hashtags_clea = []
    for a in list_hashtags:
        list_a = a.split(' ')
        for b in list_a:
            list_hashtags_clea.append(b)

    #On compte les occurences dans la nouvelle liste
    counter = Counter(list_hashtags_clea)

    #On transforme l'objet counter en dictionnaire
    dict_hashtags = dict(counter)
    
    #On met tout ça dans un dataframe
    dfgraph1 = pd.DataFrame(list(dict_hashtags.items()),columns=['Hashtags', 'count'])

    #Creation du graph count des hashtags
    fig1 = px.bar(dfgraph1, x='Hashtags', y = 'count', labels={"Hashtags": "", "count": "Nbr de hashtag"}, width=850, height=500)
    fig1.update_traces(marker_color='#FF0D63')
    fig1.update_yaxes(dtick = 1)
    fig1.update_layout(plot_bgcolor = '#F4F4F4')
    graph1JSON = json.dumps(fig1, cls =plotly.utils.PlotlyJSONEncoder)

    #Graph2:
    #Création d'un dataframe en requetant sql
    dfgraph2 = pd.read_sql("select hashtags, sentiment from tweets_live WHERE hashtags != ''", conn)
    dfgraph2 = pd.concat([pd.Series(row['sentiment'], row['hashtags'].split(' '))
                          for _, row in dfgraph2.iterrows()]).reset_index()
    dfgraph2 = dfgraph2.rename(columns={"index": "hashtags", 0: "sentiment"})
    dfgraph2 = pd.DataFrame(dfgraph2['sentiment'].value_counts()).reset_index().rename( columns={"index": "sentiment", 'sentiment': "Amount"})
    
    #Création du graphs live sentiment analysis du hashtag sélectionné
    fig2 = px.bar(dfgraph2, x='sentiment', y='Amount', labels={"sentiment": "", "Amount": "Nbr de hashtag"})
    fig2.update_traces(marker_color='#FF0D63')
    fig2.update_layout(plot_bgcolor = '#F4F4F4')
    graph2JSON = json.dumps(fig2, cls =plotly.utils.PlotlyJSONEncoder)

    #Création d'une liste avec les hashtags unique, pour le dropdown
    list_hashtags_unique = list(dict.fromkeys(list_hashtags_clea))

    #Fermeture de la db
    c.close()
    conn.close()

    #On retourne la page index.html, ainsi que tous les variables nécessaires
    return render_template('index.html', 
    list_titres = list_titres, 
    list_des = list_des, 
    list_src = list_src,
    list_link = list_link, 
    list_img = list_img, 
    list_publi = list_publi,
    list_tweets = list_tweets,
    list_username = list_username,
    list_creatime = list_creatime,
    list_impact = list_impact,
    graph1JSON = graph1JSON,
    graph2JSON = graph2JSON,
    hashtags = list_hashtags_unique)

#Lancement des scripts de récupérations des informations, avec un délai entre chaque, avant de lancer l'app
if __name__ == '__main__':
    path = os.path.dirname(__file__)
    path_pyth = os.path.join(path, "..","venv", "Scripts", "python")

    print("Launching tweets_live...")
    print("Connected to Twitter API.")
    Popen([path_pyth,'tweets_live.py'])
    for i in range(101):
        time.sleep(0.65)
        sys.stdout.write("\r%d%%" % i)
        sys.stdout.flush()
    print('tweets_live completed.')
    time.sleep(1)

    print("Launching tweets_recup...")
    print("Connected to Twitter API.")
    Popen([path_pyth,'tweets_recup.py'])
    for i in range(101):
        time.sleep(0.15)
        sys.stdout.write("\r%d%%" % i)
        sys.stdout.flush()
    print('tweets_recup completed.')
    time.sleep(1)
   
    print("Launching news_articles...")
    print("Connected to google news API.")
    Popen([path_pyth,'news_articles.py'])
    for i in range(101):
        time.sleep(0.02)
        sys.stdout.write("\r%d%%" % i)
        sys.stdout.flush()
    print('news_articles completed.')
    time.sleep(1)

    print("Launching the app...")
    app.run(debug=False)