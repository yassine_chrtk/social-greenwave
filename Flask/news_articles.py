from newsapi import NewsApiClient
from datetime import datetime, timedelta
import sqlite3
import schedule
import time
import re
import nltk

def news_article():

  #Create the authentification object
  newsapi = NewsApiClient(api_key='c66b5763557b4d97a5ef8eb39dfb00b4')

  #Création de la db s'il elle n'existe pas
  conn = sqlite3.connect('social_greenwave.db')  
  c = conn.cursor()

  #On supp la table pour préparer la nouvelle requêtes
  c.execute("DROP TABLE news_articles;")

  #titre, description, source, publishedAt, lien
  def create_table():
    c.execute('''CREATE TABLE IF NOT EXISTS news_articles
              ([titre] TEXT,
              [description] TEXT, 
              [source] TEXT,
              [publishedAt] DATETIME, 
              [lien] TEXT,
              [lienImg] TEXT)''')

  conn.commit()
  create_table()

  #Récupération des hashtags sur les tweets les plus populaires des 7 derniers jours
  data = c.execute("SELECT content, (favorite_count + retweet_count) as impact FROM tweets_recup WHERE hashtags != '' ORDER BY impact DESC LIMIT 6").fetchall()
  contents = [content[0] for content in data]

  tweets = []
  for tweet in contents:

      #Ponctuation
      tweet_ponct = re.sub(r'[^\w\s]','',tweet)
      list_mot = tweet_ponct.split(' ')
      
      #Stopword + majuscule
      stopwordsfr = nltk.corpus.stopwords.words("french")
      text_clean = [word.lower() for word in list_mot if word.lower() not in stopwordsfr]
      for word in text_clean:
          if word != '':
              tweets.append(word)

  #Fréquence d'apparition de chaque mot
  feq2 = nltk.FreqDist(tweets)

  #Les 20 plus présents
  motCom2 = feq2.most_common(40)

  #Import de la liste de mots à partir du txt et transformation pour être compris par l'API
  fichier_text = open("list_of_words.txt", "r", encoding='utf-8')
  content = fichier_text.read()
  list_of_words = content.split(" ")
  fichier_text.close()

  word_in_list = []
  for word in motCom2:
      if word[0] in list_of_words:
          word_in_list.append(word[0])

  search_words = ' OR '.join(word_in_list)

  #On délimite les dates de récupération des articles (max 1 mois avec l'API)
  date_since = (datetime.today() - timedelta(days=6)).strftime('%Y-%m-%d')
  date_until = datetime.today().strftime('%Y-%m-%d')

  #Recherche des articles
  all_articles = newsapi.get_everything(q=search_words,
                                        from_param=date_since,  
                                        to=date_until,            
                                        language='fr',
                                        sort_by= 'relevancy')

  #On récupère les infos des 6 premiers articles
  for i in range(6):
    a = all_articles['articles'][i]

    #Titre de l'article
    titre = a['title']

    #Decritpion
    description = a['description']

    #Nom du site
    source = a['source']['name']

    #Date de publication
    temp_publishedAt = datetime.strptime(a['publishedAt'], "%Y-%m-%dT%XZ")
    publishedAt = temp_publishedAt.strftime('%d-%m-%Y')

    #Url du lien
    lien = a['url']

    #Url lien image
    lienImg = a['urlToImage']

    #Ajout des infos dans la DB
    c.execute("INSERT INTO news_articles VALUES (?, ?, ?, ?, ?, ?)", 
    (titre, description, source, publishedAt, lien, lienImg))
    conn.commit()
    #print(titre, description, source, publishedAt, lien)

#Actualisation des articles toutes les 24h
schedule.every(24).hours.do(news_article)

if __name__ == '__main__':
    news_article()

    while True:
        schedule.run_pending()
        time.sleep(3600)
    
