#Import des libraries
import tweepy
from datetime import datetime, timedelta
import sqlite3
from fonctions import read_hashtags, sentiment_analysis, cleaning_tweet
import schedule
import time

def tweets_reup():

  #Twitter API credentials
  consumer_key = 'LwbgmzAq6yPaBbFSkZWFhY05G'
  consumer_secret = 'UkfIc7dzb29Q4BZ7d0mqP72r6w8Dihi5lM3xKWKQxiOwZEHK75'

  access_token = '3088339503-65iPcoHmAoSZhC3vGCVV8OWJSql57MNGz4clVUg'
  access_token_secret = 'uhUKBYw6TdJAQlsR7HvV53cdJCYIr3QNeJsKkgvx8VShd'

  #Create the authentification object
  auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
  auth.set_access_token(access_token, access_token_secret)

  #Create the API object
  api = tweepy.API(auth)

  #Création de la db s'il elle n'existe pas
  conn = sqlite3.connect('social_greenwave.db')  
  c = conn.cursor()

  #On supp la table pour préparer la nouvelle requêtes
  c.execute("DROP TABLE tweets_recup;")

  def create_table():
    c.execute('''CREATE TABLE IF NOT EXISTS tweets_recup
              ([tweetId] INTEGER,
              [Creation_time] DATETIME, 
              [username] TEXT,
              [user_verified] BOOLEAN, 
              [content] TEXT, 
              [sentiment] TEXT,
              [favorite_count] INTEGER,
              [retweet_count] INTEGER,
              [hashtags] TEXT,
              [lang] TEXT)''')

  conn.commit()
  create_table()

  #Import de la liste de mots à partir du txt
  fichier_text = open("list_of_words.txt", "r", encoding='utf-8')
  content = fichier_text.read()
  list_of_words = content.split(" ")
  fichier_text.close()

  search_words = ' OR '.join(list_of_words)

  #On délimite les dates de récupération des tweets (max 7jours avec l'API)
  date_since = (datetime.today() - timedelta(days=6)).strftime('%Y-%m-%d')
  date_until = datetime.today().strftime('%Y-%m-%d')

  #Fonction recherche des tweets
  def search_tweets(new_search, date_since):

      # performs the search using the defined variables
      for tweet in tweepy.Cursor(api.search,
                                q=new_search,
                                tweet_mode='extended',
                                lang='fr',
                                result_type='popular',
                                include_entities=True,
                                since=date_since,
                                until=date_until).items(20):

          try:

            #Si le tweet n'est pas un RT, analyse et ajout dans la DB
            if not hasattr(tweet, "retweeted_status"):

              #tweeter id
              tweet_id = tweet.id

              #Creation_time +2h, car l'API donne l'heure GMT +0
              creation_time = tweet.created_at + timedelta(hours=2)

              #Username
              username = tweet.user.screen_name

              #Compte vérifié
              user_verified = tweet.user.verified

              #Récup text et Nettoyage
              content = cleaning_tweet(tweet.full_text)

              #Analyse sentiment
              sentiment = sentiment_analysis(content)

              #Nbr de likes
              favorite_count = tweet.favorite_count

              #Nbr de RT
              retweet_count = tweet.retweet_count

              #Récup des hastags
              hashtags = read_hashtags(tweet.entities['hashtags']) 

              #langue du tweet
              lang = tweet.lang

              #On vérifie si le tweet est déjà dans la DB en la requetant à partir de son id: s'il y a pas de résultat, on rajoute le tweet
              c.execute("SELECT * FROM tweets_recup WHERE tweetId = (?)", (tweet_id,))
              result = c.fetchone()
              if not result:
                #Ajout du tweets dans la DB
                c.execute("INSERT INTO tweets_recup VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
                (tweet_id, creation_time, username, user_verified, content, sentiment, favorite_count, retweet_count, hashtags, lang))
                conn.commit()
                #print((tweet_id, creation_time, username, user_verified, content, sentiment, favorite_count, retweet_count, hashtags, lang))
        
          except Exception as e:
              print('Encountered Exception:', e)
              pass

  search_tweets(search_words, date_since)

#Actualisation des tweets toutes les 24h
schedule.every(24).hours.do(tweets_reup)

if __name__ == '__main__':

  tweets_reup()

  while True:
      schedule.run_pending()
      time.sleep(3600)


