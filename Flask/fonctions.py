from googletrans import Translator
import nltk
import re
#nltk.download('popular')
#nltk.download('vader_lexicon')
from nltk.sentiment import SentimentIntensityAnalyzer


#Fonction pour extraire les hashtags
def read_hashtags(tag_list):
    hashtags = []
    for tag in tag_list:
        hashtags.append(tag['text'])
    return ' '.join(hashtags)


stopwordsenglish = nltk.corpus.stopwords.words("english")

#Fonction de traduction + analyse de sentiment
def sentiment_analysis(text):

  #Traduction si besoin
  translator = Translator()
  detect = translator.detect(text)
  if detect.lang == 'en':
    text_trad = text
  else:
    translation = translator.translate(text, dest='en')
    text_trad = translation.text

  #Ponctuation
  out = re.sub(r'[^\w\s]','',text_trad)
  list_mot = out.split(' ')

  #Stopword + majuscule
  text_clean = [word.lower() for word in list_mot if word.lower() not in stopwordsenglish]

  #Sentiment analysis
  sia = SentimentIntensityAnalyzer()
  score = sia.polarity_scores(' '.join(text_clean))
  if score['compound'] < -0.5:
    return('Très négatif')
  elif -0.5 < score['compound'] < 0:
    return('Plutôt négatif')
  elif 0 < score['compound'] < 0.5:
    return('Plutôt positif')
  elif score['compound'] >= 0.5:
    return('Trés positif')
  else:
    return('Neutre')


#Fonction de nettoyage du tweet pour affichage web app
def cleaning_tweet(tweet):

    #Supp les liens
    tweet = re.sub(r'http\S+', '', tweet)

    #Supp les mentions
    tweet = re.sub("@\w+","",tweet)

    #Supp espaces multiples
    tweet = re.sub("\s+"," ",tweet)
    return tweet

